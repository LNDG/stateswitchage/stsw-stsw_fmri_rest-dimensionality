% Conduct PCA for STSWD YA subjects
% also save images of pre-and post-GSR BOLD dynamics

% 180221 | function adapted from MerlinDynamics for STSWD YA
% 180222 | use conjunction with grey matter mask; coords were extracted for PLS (v3)
%        | output files are appended with an additional '_GM'
% 180323 | v4: use common GM mask across OAs and YAs, load unzipped niftys

% requires common coords (PLS step) and unzipped niftis (PLS step 0)

% N = 43 YA (1126 has no Rest data) + 53 OA;
IDs = {'1117';'1118';'1120';'1124';'1125';'1131';'1132';'1135';'1136';...
    '1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1214';'1215';...
    '1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';...
    '1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';'2130';...
    '2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';'2149';'2157';...
    '2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';'2211';'2213';'2214';...
    '2215';'2216';'2217';'2219';'2222';'2224';'2226';'2227';'2236';'2237';'2238';...
    '2241';'2244';'2246';'2248';'2250';'2251';'2252';'2258';'2261'};

%% paths

pn.root             = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/rest/';
pn.savePath         = [pn.root, 'analyses/B_dimensionality/B_data/B_dataOut/'];
pn.niftyPath        = [pn.root, 'analyses/C_PLS/B_data/BOLDin/'];
pn.tools            = [pn.root, 'preproc/D_tools/']; addpath(genpath(pn.tools)); % add toolboxes
pn.dataOut          = [pn.root, 'analyses/B_dimensionality/B_data/D_Dimensionality_v1/']; mkdir(pn.dataOut);
pn.plotFolder       = [pn.root, 'analyses/B_dimensionality/C_figures/B_2D_Dynamics_prepostGSR_v2/']; mkdir(pn.plotFolder);

% load GM coordinates without zeros

load([pn.root, 'analyses/C_PLS/B_data/VoxelOverlap/coords_N96.mat'], 'final_coords_withoutZero');
st_coords = final_coords_withoutZero; clear final_coords_withoutZero;

% % load coordinates where all subjects have non-zero power
% binaryMaskFile = [pn.savePath, 'A_STSWD_YA_BinMatNonZeroPower.nii.gz'];
% st_coords = S_load_nii_2d(binaryMaskFile);

for indID = 1:numel(IDs)

    disp(IDs{indID});
    
    % load preprocessed Nifty
    fname=([pn.niftyPath,IDs{indID},'_rest_feat_detrended_bandpassed_manualdenoise_MNI3mm.nii']);
    img = double(S_load_nii_2d(fname));
    img = img(st_coords, :);

    % optional: inspect standardized dynamics
    [sortVal, sortInd] = sort(nanstd(img,[],2), 'ascend');
    h = figure; subplot(1,7,1:3);
    imagesc(zscore(img(sortInd,:),[],2)); xlabel('Volumes'); ylabel('Voxel (sorted by power)');
    set(gca,'Ydir','Normal');
    title('Z-scored BOLD dynamics')
    % global signal regression
    globalSignal = nanmean(img,1);
    tic
    residuals = [];
    for indVoxel = 1:size(img,1)
        [~, ~, residuals(:,indVoxel)] = regress(img(indVoxel,:)', (globalSignal-nanmean(globalSignal))'); % No demeaning is done here. Grave error. -JQK
    end
    toc
    residuals = residuals';
    subplot(1,7,4:6);
    imagesc(zscore(residuals(sortInd,:),[],2)); xlabel('Volumes'); ylabel('Voxel (sorted by power)');
    set(gca,'Ydir','Normal');
    title('Z-scored BOLD dynamics (following GSR)')
    % power
    subplot(1,7,7); plot(nanstd(img(sortInd,:),[],2)); xlim([0 size(img,1)])
    view([90 -90]); set(gca,'Ydir','Normal');
    title('Power')
    set(findall(gcf,'-property','FontSize'),'FontSize',18);
    
    figureName = ['A_DynamicsSortedByPower_', IDs{indID}];
%     saveas(h, [pn.plotFolder, figureName], 'fig');
%     saveas(h, [pn.plotFolder, figureName], 'epsc');
    saveas(h, [pn.plotFolder, figureName], 'png');
    close(h);
    
    %% calculate PCA dimensionality across all non-zero power voxels

    % only need the EXPLAINED output to get what's needed.
    % coeff: component coefficient scores. Later calculated by normalization
    % of scores*data. But variable is initiliased here
    try
        %% without global signal regression
        % spatial PCA using correlation matrix
        tic;[coeff_spat, scores_spat, latent_spat, ~, Explained.spat_cor] = pca(img', 'VariableWeights','variance'); toc;
        tic;[~, ~, ~, ~, Explained.spat_cor_z] = pca(zscore(img',[],2), 'VariableWeights','variance'); toc;
        % temporal PCA using correlation matrix
        tic;[coeff_temp, scores_temp, latent_temp, ~, Explained.temp_cor] = pca(img, 'VariableWeights','variance');toc;
        tic;[~, ~, ~, ~, Explained.temp_cor_z] = pca(zscore(img,[],2), 'VariableWeights','variance');toc;
        % spatial PCA using covariance matrix
        tic;[~, ~, ~, ~, Explained.spat_cov] = pca(img'); toc;
        tic;[~, ~, ~, ~, Explained.spat_cov_z] = pca(zscore(img',[],2)); toc;
        % temporal PCA using correlation matrix
        tic;[~, ~, ~, ~, Explained.temp_cov] = pca(img);toc;
        tic;[~, ~, ~, ~, Explained.temp_cov_z] = pca(zscore(img,[],2));toc;
        %% with global signal regression
        % spatial PCA on GSA data using correlation matrix
        tic;[coeff_spat_gsa, scores_spat_gsa, latent_spat_gsa, ~, ExplainedGSA.spat_cor] = pca(residuals', 'VariableWeights','variance'); toc;
        tic;[~, ~, ~, ~, ExplainedGSA.spat_cor_z] = pca(zscore(img',[],2), 'VariableWeights','variance'); toc;
        % temporal PCA on GSA data using correlation matrix
        tic;[coeff_temp_gsa, scores_temp_gsa, latent_temp_gsa, ~, ExplainedGSA.temp_cor] = pca(residuals, 'VariableWeights','variance');toc;
        tic;[~, ~, ~, ~, ExplainedGSA.temp_cor_z] = pca(zscore(residuals,[],2), 'VariableWeights','variance');toc;
        % spatial PCA on GSA data using covariance matrix
        tic;[~, ~, ~, ~, ExplainedGSA.spat_cov] = pca(residuals'); toc;
        tic;[~, ~, ~, ~, ExplainedGSA.spat_cov_z] = pca(zscore(residuals',[],2)); toc;
        % temporal PCA on GSA data using correlation matrix
        tic;[~, ~, ~, ~, ExplainedGSA.temp_cov] = pca(residuals);toc;
        tic;[~, ~, ~, ~, ExplainedGSA.temp_cov_z] = pca(zscore(residuals,[],2));toc;
        
        %% save 
        save([pn.dataOut 'A_',IDs{indID},'_Dim_woGSA_N96_GM.mat'], ...
            'coeff_spat', 'scores_spat', 'latent_spat', 'coeff_temp', 'coeff_temp', 'coeff_temp', 'Explained', 'IDs');
        save([pn.dataOut 'B_',IDs{indID},'_Dim_GSA_N96_GM.mat'], ...
            'coeff_spat_gsa', 'scores_spat_gsa', 'latent_spat_gsa', 'coeff_temp_gsa', 'coeff_temp_gsa', 'coeff_temp_gsa', 'ExplainedGSA', 'IDs');

    catch ME
        disp([ME.message, subjID])
    end

end % subject loop
