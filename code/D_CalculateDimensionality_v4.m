% Extract Dimensionality estimates to prepare for PLS

% 180221     | function adapted from MerlinDynamics for STSWD YA
% 180228(v2) | use dimensionality estimates within grey matter mask
% 180326     | include OAs

% N = 43 YA (1126 has no Rest data) + 53 OA;
IDs = {'1117';'1118';'1120';'1124';'1125';'1131';'1132';'1135';'1136';...
    '1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1214';'1215';...
    '1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';...
    '1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';'2130';...
    '2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';'2149';'2157';...
    '2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';'2211';'2213';'2214';...
    '2215';'2216';'2217';'2219';'2222';'2224';'2226';'2227';'2236';'2237';'2238';...
    '2241';'2244';'2246';'2248';'2250';'2251';'2252';'2258';'2261'};

pn.root             = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/rest/';
pn.dimData = [pn.root, 'analyses/B_dimensionality/B_data/D_Dimensionality_v1/'];

%% get PCA results

for indID = 1:numel(IDs)
    
    disp(num2str(indID));
    dataWoGSA = load([pn.dimData, 'A_',IDs{indID},'_Dim_woGSA_N96_GM.mat'],'Explained');
    dataGSA = load([pn.dimData, 'B_',IDs{indID},'_Dim_GSA_N96_GM.mat'], 'ExplainedGSA');

    %% calculate dimensionality: amount of components necessary to explain criterion % of variance

    %categories = {'spat_cor', 'spat_cor_z', 'temp_cor', 'temp_cor_z', 'spat_cov', 'spat_cov_z', 'temp_cov', 'temp_cov_z'};
    categories = {'spat_cor', 'spat_cor_z', 'spat_cov', 'spat_cov_z'};
    criterions = 10:10:100;
    
    for indCat = 1:numel(categories)
    for indCrit = 1:numel(criterions)
        cumulExplainedwoGSR = cumsum(dataWoGSA.Explained.(categories{indCat}));
        dimensionality(indCat,indCrit,indID) = find(round(cumulExplainedwoGSR)>=criterions(indCrit),1,'first'); % number of dimensions at which criterion % explained is reached or exceeded
        cumulExplainedGSR = cumsum(dataGSA.ExplainedGSA.(categories{indCat}));
        dimensionality_GSR(indCat,indCrit,indID) = find(round(cumulExplainedGSR)>=criterions(indCrit),1,'first');
    end
    end
end

save([pn.dimData, 'C_DimSubjectwise_GSR_noGSR_N96_GM.mat'], 'dimensionality', 'dimensionality_GSR', 'categories', 'criterions', 'IDs');